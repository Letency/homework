package company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage {

    private WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
    }


    private By inputEmail = By.id("email_create");
    private By selectGenderMr1 = By.id("id_gender1");
    private By selectGenderMrs2 = By.id("id_gender2");
    private By inputCustomerFirstName = By.id("customer_firstname");
    private By inputCustomerLastName = By.id("customer_lastname");
    private By inputPassword = By.id("passwd");
    private By inputFirstName = By.id("firstname");
    private By inputLastName = By.id("lastname");
    private By inputCompany = By.id("company");
    private By inputAddress1 = By.id("address1");
    private By inputAddress2 = By.id("address2");
    private By inputCity = By.id("city");
    private By inputAdditionalInfo = By.id("other");
    private By inputPhone = By.id("phone");
    private By inputMobilePhone = By.id("phone_mobile");
    private By inputAliasAddress = By.id("alias");
    private By clickRegister = By.id("submitAccount");
    private By clickSubmitCreateEmail = By.id("SubmitCreate");
    private By inputPostcode = By.id("postcode");
    private By getTextErrorAddress = By.xpath("//span[text()='Street address, P.O. Box, Company name, etc.']");

    public RegistrationPage typeEmail(String email) {
        driver.findElement(inputEmail).sendKeys(email);
        return this;
    }

    public String getErrorAddress() {
        return this.driver.findElement(getTextErrorAddress).getText();
    }

    public RegistrationPage typePostcode(String postcode) {
        driver.findElement(inputPostcode).sendKeys(postcode);
        return this;
    }

    public RegistrationPage submitCreateAccount() {
        driver.findElement(clickSubmitCreateEmail).click();
        return this;
    }

    public RegistrationPage selectGenderMr() {
        driver.findElement(selectGenderMr1).click();
        return this;
    }

    public RegistrationPage selectGenderMrs(){
        driver.findElement(selectGenderMrs2).click();
        return this;
    }

    public RegistrationPage typeCustomerFirstName(String customerFirstName) {
        driver.findElement(inputCustomerFirstName).sendKeys(customerFirstName);
        return this;
    }

    public RegistrationPage typeCustomerLastName(String customerLastName) {
        driver.findElement(inputCustomerLastName).sendKeys(customerLastName);
        return this;
    }

    public RegistrationPage typePassword(String password) {
        driver.findElement(inputPassword).sendKeys(password);
        return this;
    }

    public RegistrationPage typeFirstName(String firstName) {
        driver.findElement(inputFirstName).sendKeys(firstName);
        return this;
    }

    public RegistrationPage typeLastName(String lastName) {
        driver.findElement(inputLastName).sendKeys(lastName);
        return this;
    }

    public RegistrationPage typeCompany(String company) {
        driver.findElement(inputCompany).sendKeys(company);
        return this;
    }

    public RegistrationPage typeAddress1(String address1) {
        driver.findElement(inputAddress1).sendKeys(address1);
        return this;
    }

    public RegistrationPage typeCity(String city) {
        driver.findElement(inputCity).sendKeys(city);
        return this;
    }

    public RegistrationPage typeAdditionalInfo(String additionalInfo) {
        driver.findElement(inputAdditionalInfo).sendKeys(additionalInfo);
        return this;
    }

    public RegistrationPage typePhone(String phone) {
        driver.findElement(inputPhone).sendKeys(phone);
        return this;
    }

    public RegistrationPage typeMobilePhone(String mobilePhone) {
        driver.findElement(inputMobilePhone).sendKeys(mobilePhone);
        return this;
    }

    public RegistrationPage typeAddress2(String address2) {
        driver.findElement(inputAddress2).sendKeys(address2);
        return this;
    }

    public RegistrationPage typeAliasAddress(String aliasAddress) {
        driver.findElement(inputAliasAddress).sendKeys(aliasAddress);
        return this;
    }

    public RegistrationPage submitAccount() {
        driver.findElement(clickRegister).click();
        return this;
    }


}
