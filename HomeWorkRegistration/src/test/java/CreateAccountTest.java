import company.MainPage;
import company.RegistrationPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class CreateAccountTest {

    private MainPage mainPage;
    private RegistrationPage registrationPage;
    private WebDriver driver;

    @Before
    public void setupAccount() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        mainPage = new MainPage(this.driver);
        registrationPage = new RegistrationPage(this.driver);

    }

    @Test
    public void createNewAccount() throws InterruptedException {

        mainPage
                .openMainPage()
                .openSignIn();



        registrationPage
                .typeEmail("test116644@mail.com")
                .submitCreateAccount()
                .selectGenderMr()
                .typeCustomerFirstName("Test CustomerFirstName")
                .typeCustomerLastName("Test CustomerLastName")
                .typePassword("123456")
                .typeFirstName("Test FirstName")
                .typeLastName("Test LastName")
                .typeCompany("Hillel")
                .typeAddress1("Test Address1")
                .typeAddress2("Test Address2")
                .typeCity("Test City")
                .typePostcode("Test Zip")
                .typeAdditionalInfo("Test AdditionalInfo")
                .typePhone("Test Phone")
                .typeMobilePhone("Test MobilePhone")
                .typeAliasAddress("Test AliasAddress");

        String actualResult = registrationPage.getErrorAddress();
        String expectedResult = "Street address, P.O. Box, Company name, etc.";
        Assert.assertEquals("Street address, P.O. Box, Company name, etc.", registrationPage.getErrorAddress());







    }


    @After

    public void cleanup() {
        driver.manage().deleteAllCookies();
        driver.close();


    }

}
