import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class MainPage {

    private static final String MAIN_PAGE_URL = "http://automationpractice.com/index.php";
    private WebDriver driver;
    public MainPage(WebDriver webDriver) {
        this.driver = webDriver;
    }

    private By clickSignIn = By.xpath("//a[@title='Log in to your customer account']");
    private By inputSearch = By.id("search_query_top");
    private By submitSearch = By.xpath("//button[@name='submit_search']");

    public MainPage typeSearch(String search) {
    driver.findElement(inputSearch).sendKeys(search);
    return this;
    }

    public MainPage clickSubmitSearch() {
        driver.findElement(submitSearch).click();
        return this;
    }


    public MainPage openMainPage() {
        driver.navigate().to(MAIN_PAGE_URL);
        return this;
    }

    public MainPage openSignIn() {
        driver.findElement(clickSignIn).click();
        return this;
    }







}
