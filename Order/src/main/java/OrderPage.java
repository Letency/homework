import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.print.attribute.standard.OrientationRequested;

public class OrderPage {

    private WebDriver driver;

    public OrderPage(WebDriver webDriver) {
        this.driver = webDriver;
    }

    private By addSummary1 = By.xpath("//i[@class='icon-plus']");
    private By getTotalProducts = By.id("total_product");
    private By getTotalShipping = By.id("total_shipping");
    private By getTotal = By.id("total_price_without_tax");
    private By getTotalTax = By.id("total_tax");
    private By getTotalPrice = By.id("total_price");
    private By deleteProduct = By.xpath("//i[@class='icon-trash']");




    public OrderPage clickDeleteProduct() {
        driver.findElement(deleteProduct).click();
        return this;
    }

    public String checkTotalPrice() {
        return this.driver.findElement(getTotalPrice).getText();
    }

    public String checkTotalTax() {
        return this.driver.findElement(getTotalTax).getText();
    }

    public String checkTotal() {
        return this.driver.findElement(getTotal).getText();
    }

    public String checkTotalShipping() {
        return this.driver.findElement(getTotalShipping).getText();
    }

    public String checkTotalProducts() {
        return this.driver.findElement(getTotalProducts).getText();
    }

    public OrderPage clickAddSummary1() {
        driver.findElement(addSummary1).click();
        return this;
    }


}
