import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {

    private WebDriver driver;

    public SearchResultPage(WebDriver webDriver) {
        this.driver = webDriver;
    }

    private By modeList = By.xpath("//i[@class='icon-th-list']");
    private By addToCart = By.xpath("//a[@class='button ajax_add_to_cart_button btn btn-default']");
    private By proceedToCheckout = By.xpath("//a[@class='btn btn-default button button-medium']");
    private By getTextCart = By.xpath("//p[@class='alert alert-warning']");

    public String checkCart() {
            return this.driver.findElement(getTextCart).getText();
        }

        public SearchResultPage clickModeList() {
        driver.findElement(modeList).click();
        return this;
    }

    public SearchResultPage clickAddToCart() {
        driver.findElement(addToCart).click();
        return this;
    }

    public SearchResultPage clickProceedToCheckout() {
        driver.findElement(proceedToCheckout).click();
        return this;
    }

    


}
