import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class CheckCartTest {

    private MainPage mainPage;
    private WebDriver driver;
    private SearchResultPage searchResultPage;
    private OrderPage orderPage;


    @Before
    public void setupAccount() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        mainPage = new MainPage(this.driver);
        searchResultPage = new SearchResultPage(this.driver);
        orderPage = new OrderPage(this.driver);
    }

    @Test
    public void CheckCart() throws InterruptedException {

        mainPage.openMainPage()
                .typeSearch("Blouse")
                .clickSubmitSearch();

        searchResultPage.clickModeList()
                .clickModeList()
                .clickAddToCart()
                .clickProceedToCheckout();

        orderPage.clickAddSummary1();

        Thread.sleep(5000);

        String actualResultTotalProducts = orderPage.checkTotalProducts();
        String expectedResultTotalProducts = "$54.00";
        Assert.assertTrue("$54.00", actualResultTotalProducts.contains(expectedResultTotalProducts));

        String actualResultTotalShipping = orderPage.checkTotalShipping();
        String expectedResultTotalShipping = "$2.00";
        Assert.assertTrue("$2.00", actualResultTotalShipping.contains(expectedResultTotalShipping));

        String actualResultTotal = orderPage.checkTotal();
        String expectedResultTotal = "$56.00";
        Assert.assertTrue("$56.00", actualResultTotal.contains(expectedResultTotal));

        String actualResultTotalTax = orderPage.checkTotalTax();
        String expectedResultTotalTax = "$0.00";
        Assert.assertTrue("$0.00", actualResultTotalTax.contains(expectedResultTotalTax));

        String actualResultTotalPrice = orderPage.checkTotalPrice();
        String expectedResultTotalPrice = "$56.00";
        Assert.assertTrue("$56.00", actualResultTotalPrice.contains(expectedResultTotalPrice));

        orderPage.clickDeleteProduct();

        Thread.sleep(3000);

        String actualResultCartIsEmpty = searchResultPage.checkCart();
        String expectedResultCartIsEmpty = "Your shopping cart is empty.";
        Assert.assertTrue("Your shopping cart is empty.", actualResultCartIsEmpty.contains(expectedResultCartIsEmpty));


    }




    @After
    public void cleanup() {

        driver.manage().deleteAllCookies();
        driver.close();

    }
}